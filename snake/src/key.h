#ifndef __KEY_H__
#define __KEY_H__

#include "head.h"




typedef enum{
    KEY_UP = 0,
    KEY_DOWN,
    KEY_LEFT,
    KEY_RIGHT,
    KEY_OK,
    KEY_NUM = 5,
    KEY_OK_LONG = 10,
   KEY_NULL,
}Key_Status_t;

Key_Status_t Key_Scanf(void);
void Key_Handel(Key_Status_t key);
#endif

