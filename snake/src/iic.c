#include "iic.h"




void IIC_Send(uint8_t c)
{
    uint8_t i;
    IIC_SCL = 0;
    for(i=0; i<8; i++)
    {
        if(c&0x80)
            IIC_SDA = 1;
        else
            IIC_SDA = 0;

        c<<=1;
        IIC_SCL = 1;
        IIC_SCL = 0;
    }
}

void IIC_WaitAck(void)
{
   
    IIC_SCL=1;
    IIC_SCL=0;
}


void IIC_Start(void)
{
    IIC_SCL = 1;
    IIC_SDA = 1;
    IIC_SDA = 0;
    IIC_SCL = 0;



}

void IIC_Stop(void)
{
    IIC_SCL = 1;
    IIC_SDA = 0;
    IIC_SDA = 1;

}

void IIC_Write_cmd(uint8_t cmd)
{

#ifdef IIC_MODE
    

    return;
#endif

    IIC_Start();

    IIC_Send(0x78);
    IIC_WaitAck();

    IIC_Send(0X00);
    IIC_WaitAck();
    IIC_Send(cmd);
    IIC_WaitAck();
    IIC_Stop();
}


void IIC_Write_data(uint8_t data)
{
#ifdef IIC_MODE


    return;
#endif

    IIC_Start();

    IIC_Send(0x78);
    IIC_WaitAck();

    IIC_Send(0x40);
    IIC_WaitAck();
    IIC_Send(data);
    IIC_WaitAck();
    IIC_Stop();
}


void IIC_delay(uint16_t i)
{
//   	while(i--)
//   	{__NOP();}
    while(i>0)
    {
        i--;
    };
}




