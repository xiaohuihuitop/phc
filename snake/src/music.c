#include "music.h"

/// 使用比较输出 io口翻转 频率是所需频率的2倍即可 3m/443 = 6772 ——->3.3k

/// 需要比较低的频率 
/// 定时器 最高频率 262*2*1024 = 536k
/// 该mcu 使用 12/64 = 187.5k

/// 0 1 2 3 4 5 6
/*
	DO (C) 262 523 1046
	RE (D) 294 587 1175
	MI (E) 330 659 1318
	FA (F) 349 698 1397
	SO (G) 392 784 1568
	LA (A) 440 880 1760
	SI (B) 494 988 1967
*/

/// 低音周期 /2 得到中音 /4得到高音 周期越小 频率越高
const uint16_t MUSCI_FREQ[7] = {357,318,284,268,239,213,189};



const uint8_t BGM_Buff[96] = {1,2,2,2,2,2,3,2,2,1,2,2,
								1,2,2,2,2,2,3,2,2,1,2,2,
								3,2,2,4,2,2,5,2,4,                       
								3,2,2,4,2,2,5,2,4,
								0,1,5};///后3个 分割下 没有意义
//								5,2,1,6,2,1,5,2,1,4,2,1,3,2,2,1,2,2,   
//								5,2,1,6,2,1,5,2,1,4,2,1,3,2,2,1,2,2,
//								2,2,2,5,1,2,1,2,4,   
//								2,2,2,5,1,2,1,2,4  };
uint8_t BGM_Len = 45; //96

//const uint8_t BGM_Buff[40] = {1,2,3,1,1,2,3,1,3,4,5,5,3,4,5,5,5,6,5,4,3,1,5,6,5,4,3,1,1,5,1,1,1,5,1,1};
//uint8_t BGM_Len = 40;



uint8_t BGM_Flag = 0;

/*
	note 0--6 
	tone 0--3   1低音 2中音 3高音
*/
void MUSIC_Set(uint8_t note,uint8_t tone)
{
	if(note > 6)
		return;
	if(tone == 1)
	{
		_tm1al = MUSCI_FREQ[note]&0x0ff;	  
		_tm1ah = MUSCI_FREQ[note]>>8;
	}
	else if(tone == 2)
	{
		_tm1al = (MUSCI_FREQ[note]>>1)&0x0ff;	  
		_tm1ah = (MUSCI_FREQ[note]>>1)>>8;
	}
	else if(tone == 3)
	{
		_tm1al = (MUSCI_FREQ[note]>>2)&0x0ff;	  
		_tm1ah = (MUSCI_FREQ[note]>>2)>>8;
	}
	else
	{

	}
}



void BGM_En(uint8_t en)
{
	BGM_Flag = en;
}

/// 每个音 需要3个成员表示 
/// m:音符 p:低中高 t:节拍数
void BGM_Task(void)
{
	static uint8_t index = 0;
	static uint16_t interval = 0;
	static uint8_t m=0,p=0,t=0;
	if(!BGM_Flag)
	{
		Buzzer_OnOff(0);
		return;
	}
		

	
	
	if(Get_SysTick_1ms() - interval < 200)
		return;
	interval = Get_SysTick_1ms();


	m = BGM_Buff[index];
	p = BGM_Buff[index+1];
	if(t == 0)
		t = BGM_Buff[index+2];



	
	MUSIC_Set(m,p);
	
	
	Buzzer_OnOff(1);

	t--;
	if(t==0)
	{
		index+=3;
	}
	if(index >= BGM_Len)
		index = 0;
}



void Music_Test(void)
{
	uint8_t i=0;
	while(1)
	{
		MUSIC_Set(i++,2);
		if(i>=7)
			i=0;
		
		Buzzer_OnOff(1);
		delay_ms(250);
		Buzzer_OnOff(0);
		delay_ms(250);
	}
}
