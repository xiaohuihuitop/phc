#ifndef __IIC_H__
#define __IIC_H__

#include "head.h"

//#define  IIC_MODE

#define IIC_SDA     _pb5
#define IIC_SCL      _pb6


//extern unsigned char  show1[];
//extern unsigned char  show2[];
//extern unsigned char  show3[];

//extern unsigned char  t[];
//extern unsigned char  c[];
//extern unsigned char  h[];

void IIC_Init(void);

void IIC_Send(uint8_t c);
void IIC_WaitAck(void);
void IIC_Start(void);
void IIC_Stop(void);
void IIC_Write_cmd(uint8_t cmd);
void IIC_Write_data(uint8_t data);
void IIC_delay(uint16_t i);








#endif






