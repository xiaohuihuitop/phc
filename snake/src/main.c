/********************************************/

#include    "head.h"
#include    "snake.h"
#include    "key.h"
#include "led.h"
#include "music.h"
//******************************************

//-----------------------

volatile uint16_t Sys_Tick_1s = 0;// //1s
volatile uint16_t Sys_Tick_1ms = 0;// 1ms  //最大间隔不超过 1分钟


volatile uint16_t Buzzer_Time = 0;//蜂鸣器响的时间

volatile uint8_t Show_Time_Flag = 0;


volatile Sys_Status_t Sys_Status = SYS_OFF;

//---------------------------------

void main(void)
{
    Key_Status_t key;
    _nop();
    _nop();
    while(!_hto);
    _emi = 0;//先关总中断
    Sys_Init();
    _emi = 1;//开总中断
    _nop();


  // Buzzer_sound(100);

    Lcd_OFF();
    SSD1306();  //lcd  初始化
    delay_ms(1000);
    Fill(0,7,0x00); /// 填充
    Fill(0,7,0xff); /// 填充
    Lcd_ON();

    Fill(0,7,0x00); /// 填充 全黑
    delay_ms(200);

	while(Key_Scanf() != KEY_OK);
	//Music_Test();
	
    Snake_init();
	while(1)
	{
		key = Key_Scanf();
        //Key_Handel(key);
        Snake_Handle(key);
		Snake_Move();
		BGM_Task();
	}
	

        
 
}



void __attribute((interrupt(0x0C))) Timer0_ISR(void) //100us  TM0   STM
{

    static uint8_t ms1 = 0;
    static uint16_t s1 = 0;
    _t0af = 0;//清空定时器的标志位


    ms1++;
    s1++;


    if(ms1 >= 10)
    {
        ms1 = 0;
        Sys_Tick_1ms++;//系统基时
        
        //Task_Buzzer();//蜂鸣器任务
        
        
    }

    if(s1 >= 10000)
    {
        s1 = 0;
        Sys_Tick_1s++;//系统基时
        Show_Time_Flag = 1;
    }
}


uint16_t  Get_SysTick_1s(void)
{
    return Sys_Tick_1s;
}



uint16_t Get_SysTick_1ms(void)
{
    return Sys_Tick_1ms;
}

void delay_ms(uint16_t ms)
{
     uint16_t temp = Sys_Tick_1ms;
     while((Sys_Tick_1ms - temp  < ms))
     {
        //_nop();
     }
}


void Buzzer_OnOff(unsigned char On)
{
    if(On)
    {
        _t1on = 1;//启动
    }
    else
    {
        _t1on = 0;//启动
        _nop();
        _nop();
        _t1on = 1;//_tm1c0 |= (1<<3);//按规格书TnON需要从L到H才会使输出脚回到初始电平
        _nop();
        _nop();
        _t1on = 0;//_tm1c0 &= ~(1<<3);//停止
    }
}

//设置 蜂鸣器响的时间 调用此函数即可启动蜂鸣器 参数*10ms
void Buzzer_sound(unsigned char count)
{
    Buzzer_Time = count*10;
    Buzzer_OnOff(TRUE);
}


////1ms 一次 可以放到定时器中
void Task_Buzzer(void) //蜂鸣器任务
{
    if(Buzzer_Time == 0)
        return;
    if(Buzzer_Time > 1)
    {
        Buzzer_Time--;
    }
    if(Buzzer_Time == 1)
    {
        Buzzer_Time = 0;
        //Buzzer_OnOff(FALSE);  //这样会有一个警告
        _t1on = 0;//启动
        _nop();
        _nop();
        _t1on = 1;//_tm1c0 |= (1<<3);//按规格书TnON需要从L到H才会使输出脚回到初始电平
        _nop();
        _nop();
        _t1on = 0;//_tm1c0 &= ~(1<<3);//停止
    }
}

uint16_t  Buzzer_Get(void)
{
    return Buzzer_Time;
}



////读取 ad值
//uint16_t ADC_read(unsigned char ch)
//{
//    uint16_t adcVal;

//    _adcen = 1;//_sadc0 |= (1<<5);
//    _sadc0 &= 0b11111000;
//    _sadc0 |= ch;
//    _start = 0;
//    _start = 1;
//    _start = 0;//start信号0->1->0启动AD转换
//    while(_adbz == 1);
//    //_clrwdt();
//    adcVal = _sadoh;
//    adcVal <<= 8;
//    adcVal &= 0xf00;
//    adcVal += _sadol;
//    _adcen = 0;//_sadc0 &= ~(1<<5);

//    return adcVal;
//}



 








