#ifndef __MUSIC_H__
#define __MUSIC_H__

#include "head.h"

void MUSIC_Set(uint8_t note,uint8_t tone);
void BGM_Task(void);
void BGM_En(uint8_t en);
void Music_Test(void);


#endif


