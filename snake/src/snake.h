#ifndef __SNAKE_H__
#define __SNAKE_H__

#include "head.h"
#include "key.h"

//----------------------

typedef enum{
	Dir_NULL = 0,
	Dir_UP,
	Dir_DOWN,
	Dir_LEFT,
	Dir_RIGHT,
}Snake_Dir;

void test(uint8_t x,uint8_t y,uint8_t *data);


void Snake_Clean_Gram(void);
void Snake_DrawPoint(uint8_t x,uint8_t y,uint8_t mode);
void Snake_RrawWin(uint8_t xs,uint8_t ys,uint8_t xn,uint8_t yn);

void Snake_Refresh_Gram(void);
void Snake_init(void);
void Paint_Body(uint8_t x,uint8_t y,uint8_t mode);

void Snake_Handle(Key_Status_t key);
void Snake_Move(void);
void Create_Food(void);
uint8_t IS_Eat_Food(void);



#endif

