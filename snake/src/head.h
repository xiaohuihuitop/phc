//********************************************

//********************************************

#ifndef __HEAD_H__
#define __HEAD_H__

#include    "HT66F0185.h"



/* 宏定义 */
#define	uint8_t  unsigned char	//数据类型
#define	uint16_t unsigned short

#define TRUE  1
#define FALSE 0


typedef enum{
    SYS_OFF = 0, 
    SYS_INIT,
    SYS_IDLE,
    SYS_STANDBY,
    SYS_RUNSET,
    SYS_RUNNING,
    SYS_RUNOVER,
}Sys_Status_t;


extern volatile Sys_Status_t Sys_Status;

//=====================================================






//====================================================





//***********************************
void Sys_Init(void);
void clr_all_ram(void);







void Buzzer_sound(unsigned char count);
void Buzzer_OnOff(unsigned char On);
void Task_Buzzer(void); //蜂鸣器任务
uint16_t  Buzzer_Get(void);

void delay_ms(uint16_t ms);
uint16_t Get_SysTick_1ms(void);
uint16_t  Get_SysTick_1s(void);

#endif


//**********************************

