#ifndef __LED_H__
#define __LED_H__

#include "head.h"

void  SSD1306(void);
void Fill(uint8_t start,uint8_t end,unsigned char dat1);
void Lcd_ON(void);
void Lcd_OFF(void);


void SET_POS(uint16_t x,uint16_t y);
void ShowCH(uint16_t x,uint16_t y,unsigned char *s);
void ShowGB1616(uint16_t x,uint16_t y,unsigned char c[2]);
void ShowChar(uint16_t x,uint16_t y,signed char c);
void Colour_Reverse(uint16_t x,uint16_t y,uint8_t colour);

void Set_Roll(uint8_t direction,uint8_t y_start,uint8_t y_end,uint8_t speed);

void OLED_DrawBMP(unsigned char BMP[]);
void OLED_Draw(uint8_t x,uint8_t y,uint8_t data[],uint8_t size);


#endif


