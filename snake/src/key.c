#include "key.h"

Key_Status_t Key_Scanf(void)
{
    uint8_t i = 0;
    uint8_t key_status[KEY_NUM];
    static uint8_t key_step[KEY_NUM];
    static uint16_t interval = 0;
    key_status[0] = _pc6;
    key_status[1] = _pc3;
    key_status[2] = _pc2;
    key_status[3] = _pc5;
    key_status[4] = _pc4;

    for(i=0;i<KEY_NUM;i++)
    {
        if(key_step[i] != 0)
            break;
    }

    if(i >= KEY_NUM) //没有其他的按键在处理
    {
        // 判断 前面读取的按键状态中 有没有按键按下
        for(i=0;i<KEY_NUM;i++)
        {
            if(key_status[i] == 0)
            {
                key_step[i] = 1;
                interval = Get_SysTick_1ms();
                break;
            }
        }
    }
    else  //已经有按键需要处理了 
    {
        switch(key_step[i])
        {
            case 1:
                if(Get_SysTick_1ms() - interval >= 10)
                {
                    if(key_status[i] == 0)
                    {
                        key_step[i]++;
                        interval = Get_SysTick_1ms();
                    }
                    else
                    {
                        key_step[i]--;
                    }
                }
                break;
            case 2:
                if(key_status[i] != 0)
                {
                    key_step[i] = 0;
                    return (Key_Status_t)i;
                }
                else if(Get_SysTick_1ms() - interval >= 777)  // 长按触发
                {
                    key_step[i] ++;
                    interval = Get_SysTick_1ms();
                }
                
                break;
            case 3:
                if(i==KEY_OK)
                {
                    key_step[i]++;
                    return KEY_OK_LONG;
                }
                else
                {
                    if(key_status[i] == 0)
                    {
                        if(Get_SysTick_1ms() - interval >= 111)
                        {
                            interval = Get_SysTick_1ms();
                            return (Key_Status_t)i;
                        }
                    }
                    else
                    {
                        key_step[i]++; // 或者直接 为0
                    }
                }
                break;
            case 4:
                if(key_status[i] == 1)
                    key_step[i] = 0;
                break;
            default:
                key_step[i] = 0;
                break;
        }
    }

    return KEY_NULL;
}


void Key_Handel(Key_Status_t key)
{
    if(key == KEY_NULL)
        return;
          
    switch(key)
    {
        case KEY_UP:
			MUSIC_Set(0,1);
            break;
        case KEY_DOWN:
			MUSIC_Set(2,1);
            break;
        case KEY_LEFT:
			MUSIC_Set(3,1);
            break;
         case KEY_RIGHT:
			MUSIC_Set(4,1);
            break;
        case KEY_OK:
			MUSIC_Set(1,1);
            break;
        case KEY_OK_LONG:
			Sys_Status = SYS_RUNSET;

            break;
        default:
            break;
    }
	Buzzer_sound(10);
}
