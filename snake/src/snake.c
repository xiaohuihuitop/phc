#include "snake.h"
#include "key.h"
#include "led.h"
#include "music.h"
// 32*32

#define FRAME_X 48
#define FRAME_Y 2

#define FRAME_Y_SIZE 4
#define FRAME_X_SIZE 32

//  一次移动的像素为蛇的尺寸 需要与边界-匹配
#define SNAKE_X_SIZE 3
#define SNAKE_Y_SIZE 3

// 坐标与大小和范围要匹配 : Snake_Head_x = SNAKE_X_SIZE*X+1
uint8_t Snake_Head_x = 16;
uint8_t Snake_Head_y = 16;
uint8_t Food_seed_x;
uint8_t Food_seed_y;


//[0]0 1 2 3 ... 32
//[1]0 1 2 3 ... 32
//[2]0 1 2 3 ... 32
//[3]0 1 2 3 ... 32
static volatile uint8_t Snake_GRAM[FRAME_Y_SIZE][FRAME_X_SIZE] __attribute__ ((at(0x180))); ///32个 4成员数组 前面是列 后面是行

static volatile Snake_Dir s_dir = Dir_NULL;

//--------------------------------------------




//更新显存到Snake
void Snake_Refresh_Gram(void)
{
	uint8_t y;
	for(y=0;y<FRAME_Y_SIZE;y++)
	{
		OLED_Draw(FRAME_X,y+FRAME_Y,Snake_GRAM[y],FRAME_X_SIZE);
	}
}

// 0灭 1亮 写入数组 但是未刷新
void Snake_DrawPoint(uint8_t x,uint8_t y,uint8_t mode)
{
	uint8_t y_sector,y_bit,y_data;

	if(x>=32 || y>=32)
		return;

	y_sector = y/8;
	y_bit = y%8;
	y_data = Snake_GRAM[y_sector][x];
	if(mode == 0)
	{
		y_data &= ~(0x01<<y_bit);
	}
	else
	{
		y_data |= 0x01<<y_bit;
	}
	Snake_GRAM[y_sector][x] = y_data;
}


//------------------------------------
void Snake_Clean_Gram(void)
{
	uint8_t x,y;
//	for(y=0;y<32;y++)
//	for(x=0;x<32;x++)
//	{
//		Snake_DrawPoint(x,y,0);
//	}
	for(y=0;y<4;y++)
	for(x=0;x<32;x++)
	{
		Snake_GRAM[y][x] = 0;
	}	
}

/*
	起始坐标xs,ys
	长度xl,yl
	长度包含了起始坐标 最大32
*/
void Snake_RrawWin(uint8_t xs,uint8_t ys,uint8_t xl,uint8_t yl)
{
	uint8_t x,y;
	for(x=xs;x<xl;x++)
	{
		Snake_DrawPoint(x,ys,1);
		Snake_DrawPoint(x,ys+yl-1,1);
	}
	for(y=ys;y<yl;y++)
	{
		Snake_DrawPoint(xs,y,1);
		Snake_DrawPoint(xs+xl-1,y,1);
	}
}


/*
	起始坐标xs,ys
	结束坐标xn,yn
	坐标范围 0,0 到 31,31 
	可以用来画线和窗口
*/
void Snake_RrawLine(uint8_t xs,uint8_t ys,uint8_t xn,uint8_t yn)
{
	uint8_t x,y;
	for(x=xs;x<=xn;x++)
	{
		Snake_DrawPoint(x,ys,1);
		Snake_DrawPoint(x,yn,1);
	}
	for(y=ys;y<=yn;y++)
	{
		Snake_DrawPoint(xs,y,1);
		Snake_DrawPoint(xn,y,1);
	}
}


void Snake_init(void)
{
	uint8_t x,y;

	Snake_Clean_Gram();
	//画框
	Snake_RrawLine(0,0,31,31);

	//画蛇
	Paint_Body(Snake_Head_x,Snake_Head_y,1);


	// 食物
	Create_Food();
	

	// 显示
	Snake_Refresh_Gram();
}

/*
	画蛇 SNAKE_X_SIZE*SNAKE_Y_SIZE
	1,1为原点(外部界限占用了一个像素) 向右下方补点 
	如果用1个像素画蛇 范围就是 1,1 到 30,30
	坐标范围 1,1 31-SNAKE_X_SIZE 31-SNAKE_Y_SIZE
*/
void Paint_Body(uint8_t x,uint8_t y,uint8_t mode)
{
	uint8_t i,j;

	if((x>31-SNAKE_X_SIZE) || (y>31-SNAKE_Y_SIZE))
		return;
	if((x<1) || (y<1))
		return;
	
	for(j=0;j<SNAKE_Y_SIZE;j++)//y
	for(i=0;i<SNAKE_X_SIZE;i++)//x
	{
		if(mode)
		{
			Snake_DrawPoint(x+i,y+j,1);
		}
		else
		{
			Snake_DrawPoint(x+i,y+j,0);
		}
		
	}
}


void test(void)
{
	uint8_t x,y;
	for(y=0;y<32;y+=3)
	for(x=0;x<32;x++)
	{
		Paint_Body(x,y,1);
		Snake_Refresh_Gram();
	}
}

void Snake_Handle(Key_Status_t key)
{
	if(key == KEY_NULL)
		return;
	//Buzzer_sound(10);
	switch(key)
	{
		case KEY_UP:
			if(s_dir != Dir_DOWN)
				s_dir = Dir_UP;
		break;
		case KEY_DOWN:
			if(s_dir != Dir_UP)
				s_dir = Dir_DOWN;
		break;
		case KEY_LEFT:
			if(s_dir != Dir_RIGHT)
				s_dir = Dir_LEFT;
		break;
		case KEY_RIGHT:
			if(s_dir != Dir_LEFT)
				s_dir = Dir_RIGHT;
		break;
		default:
			if(s_dir == Dir_NULL)
			{
				BGM_En(0);
			}
			else
			{
				BGM_En(1);
			}
			s_dir = Dir_NULL;
		break;
	}
}


void Snake_Move(void)
{
	static uint16_t interval = 0;
	if(Get_SysTick_1ms() - interval < 200)
		return;
	interval = Get_SysTick_1ms();
	
	if(s_dir == Dir_NULL)
	{
		return;
	}
	//Buzzer_sound(10);
	Paint_Body(Snake_Head_x,Snake_Head_y,0);
	switch(s_dir)
	{
		case Dir_UP:
			if(Snake_Head_y > 1)
				Snake_Head_y-=SNAKE_Y_SIZE;
			else
				Snake_Head_y=31-SNAKE_Y_SIZE;
			
		break;
		case Dir_DOWN:
			if(Snake_Head_y < 31-SNAKE_Y_SIZE)
				Snake_Head_y+=SNAKE_Y_SIZE;
			else
				Snake_Head_y=1;

		break;
		case Dir_LEFT:
			if(Snake_Head_x > 1)
				Snake_Head_x-=SNAKE_X_SIZE;
			else
				Snake_Head_x=31-SNAKE_X_SIZE;

		break;
		case Dir_RIGHT:
			if(Snake_Head_x < 31-SNAKE_X_SIZE)
				Snake_Head_x+=SNAKE_X_SIZE;
			else
				Snake_Head_x=1;

		break;
		default:

		break;
	}
	Paint_Body(Snake_Head_x,Snake_Head_y,1);
	if(IS_Eat_Food())
	{
		Create_Food();
	}
	Snake_Refresh_Gram();
}


/*
	取余操作 确定坐标不会超过上限
	然后判断下限 确定食物出现在规定位置

	判断食物与头部的位置 确定不会重合
*/
void Create_Food(void)
{
	uint16_t temp = 0;
	uint8_t flag = 0;
	while(1)
	{
		delay_ms(1); 
		temp = Get_SysTick_1ms();
		Food_seed_x = temp%(31-SNAKE_X_SIZE+1);
		if(Food_seed_x<1)
		{
			Food_seed_x = 1;
		}
		
		if((Food_seed_x%SNAKE_X_SIZE==1))
		{

		}
		else
		{
			continue;  /// 让x坐标符合落点要求
		}

		if(Food_seed_x > Snake_Head_x)
		{
			if(Food_seed_x - Snake_Head_x >= SNAKE_X_SIZE)
			{
				flag = 1;
			}
		}
		else
		{
			if(Snake_Head_x - Food_seed_x >= SNAKE_X_SIZE)
			{
				flag = 1;
			}
		}
		break;
	}

	while(1)
	{
		delay_ms(Get_SysTick_1ms() % (31-SNAKE_Y_SIZE+1)); ///Y延时需要随机
		temp = Get_SysTick_1ms();
		Food_seed_y = temp%(31-SNAKE_Y_SIZE+1);
		if(Food_seed_y<1)
		{
			Food_seed_y = 1;
		}
		
		if((Food_seed_y%SNAKE_Y_SIZE==1))
		{

		}
		else
		{
			continue;  /// 让y坐标符合落点要求
		}
		if(flag == 1)
		{
			break;	
		}
		if(Food_seed_y > Snake_Head_y)
		{
			if(Food_seed_y - Snake_Head_y >= SNAKE_Y_SIZE)
			{
				break;
			}
		}
		else
		{
			if(Snake_Head_y - Food_seed_y >= SNAKE_Y_SIZE)
			{
				break;
			}
		}
	}

	Paint_Body(Food_seed_x,Food_seed_y,1);
}

uint8_t IS_Eat_Food(void)
{
	if((Snake_Head_x == Food_seed_x) && (Snake_Head_y == Food_seed_y))
	{
		return 1;
	}
	else
	{
		return 0;
	}
}

