#include "iic.h"
#include "font.h"



//void IIC_Init(void)
//{
// #ifdef IIC_MODE
//    

//    return;
//#endif

//    GPIO_SetMode(P3,BIT4,GPIO_PMD_OUTPUT);  // iic
//    GPIO_SetMode(P3,BIT5,GPIO_PMD_OUTPUT);  // iic
//}

void IIC_Send(uint8_t c)
{
    uint8_t i;
    IIC_SCL = 0;
    for(i=0; i<8; i++)
    {
        if(c&0x80)
            IIC_SDA = 1;
        else
            IIC_SDA = 0;

        c<<=1;
        IIC_SCL = 1;
        IIC_SCL = 0;
    }
}

void IIC_WaitAck(void)
{
   
    IIC_SCL=1;
    IIC_SCL=0;
}


void IIC_Start(void)
{
    IIC_SCL = 1;
    IIC_SDA = 1;
    IIC_SDA = 0;
    IIC_SCL = 0;



}

void IIC_Stop(void)
{
    IIC_SCL = 1;
    IIC_SDA = 0;
    IIC_SDA = 1;

}

void IIC_Write_cmd(uint8_t cmd)
{

#ifdef IIC_MODE
    

    return;
#endif

    IIC_Start();

    IIC_Send(0x78);
    IIC_WaitAck();

    IIC_Send(0X00);
    IIC_WaitAck();
    IIC_Send(cmd);
    IIC_WaitAck();
    IIC_Stop();
}


void IIC_Write_data(uint8_t data)
{
#ifdef IIC_MODE


    return;
#endif

    IIC_Start();

    IIC_Send(0x78);
    IIC_WaitAck();

    IIC_Send(0x40);
    IIC_WaitAck();
    IIC_Send(data);
    IIC_WaitAck();
    IIC_Stop();
}


void IIC_delay(uint16_t i)
{
//   	while(i--)
//   	{__NOP();}
    while(i>0)
    {
        i--;
    };
}



void  SSD1306(void)
{
    IIC_Write_cmd(0xAE);    //display off   //AE 显示关闭  AF显示打开
    IIC_Write_cmd(0x00);    //set lower column address  设置页模式下 列起始地址的 低4位   00-0f
    IIC_Write_cmd(0x10);    //set higher column address  设置页模式下 列起始地址的 高4位   10--1f
    IIC_Write_cmd(0x40);    //设置起始行

    IIC_Write_cmd(0xB0);    //set page address  设置 页地址  B0--B7

    IIC_Write_cmd(0x81);    //对比度
    IIC_Write_cmd(0xFF);    //128

    IIC_Write_cmd(0xA1);    //set segment remap  0xA0左右反置（复位值）   0xA1正常（重映射值）
    IIC_Write_cmd(0xA6);    //normal / reverse    //正常显示 数据0为关  如果是A7就是反过来

    IIC_Write_cmd(0xA8);    //multiplex ratio  //设置多路传输比率
    IIC_Write_cmd(0x3F);    //duty = 1/64

    IIC_Write_cmd(0xC8);    //Com scan direction  //设置列输出扫描方向   -- Set COM / Row Scan Direction   0xc0上下反置（复位值） 0xC8正常（重映射值）

    IIC_Write_cmd(0xD3);    //set display offset  设置显示偏移 -- set display offset (0x00~0x3F)
    IIC_Write_cmd(0x00);     // 不偏移

    IIC_Write_cmd(0xD5);    //set osc division   设置 时钟分频
    IIC_Write_cmd(0x80);    //   1---16   越大 频率越快

    IIC_Write_cmd(0xD8);    //set pre-charge period  设置预充电期间的持续时间 -- set pre-charge period
    IIC_Write_cmd(0x05);
    
    IIC_Write_cmd(0xD9);    //set pre-charge period  设置预充电期间的持续时间 -- set pre-charge period
    IIC_Write_cmd(0xF1);   // 设置 预充电 5个周期   释放1个周期





    IIC_Write_cmd(0xDA);    //set COM pins  很多选择  与 硬件有关  对图形影响大
    IIC_Write_cmd(0x12);

    IIC_Write_cmd(0xDB);//Set VcomH    // 调整VCOMH调节器的输出 -- set vcomh (0x00 / 0x20 / 0x30)
    IIC_Write_cmd(0x30);

    IIC_Write_cmd(0x8D);/*set charge pump enable*/  // 电荷泵设置
    IIC_Write_cmd(0x14);   // 开启

//    IIC_Write_cmd(0x20);  //设置 内存地址 模式
//    IIC_Write_cmd(0x02);  //设置 内存地址 模式 位页地址模式  00水平模式  01  02

    IIC_Write_cmd(0xAF);    //display ON

}


/*
    00 : HEI    ff: BAI
*/
void Fill(uint8_t start,uint8_t end,unsigned char dat1)
{
    unsigned char i,j;

    for(j=start; j<=end; j++)
    {
        IIC_Write_cmd(0xB0+j);    	//set page address
        IIC_Write_cmd(0x00+0);    	//set lower column address
        IIC_Write_cmd(0x10);    	//set higher column address
        for(i=0; i<128; i++)          //虽然 132 只需要右移动 2位 但是可能屏幕上有缓存 在滚动时会出现乱码
        {
            IIC_Write_data(dat1);
        }
    }
}

void Lcd_OFF(void)
{
    IIC_Write_cmd(0x8D);/*set charge pump enable*/  // 电荷泵设置
    IIC_Write_cmd(0x10);   // 关闭
    IIC_Write_cmd(0XAE);// 关闭
}

void Lcd_ON(void)
{
    IIC_Write_cmd(0x8D);/*set charge pump enable*/  // 电荷泵设置
    IIC_Write_cmd(0x14);   // 开启
    IIC_Write_cmd(0XAF);//显示
}

//void test(void)
//{
//    uint16_t i,t=0;
//    for(i=0; i<1; i++)
//        delay_ms(10);
//    Fill(0,7,0x00); /// 填充 
//    ShowCH(25,0,"-贵之族生-");
//    
//  
//    ShowCH(10,4,"TEL-1234567890");
//    while(1)
//    {
//        if(t++%2==0)
//           IIC_Write_cmd(0xA6);  
//        else 
//            IIC_Write_cmd(0xA7);  
//        
//        for(i=0; i<500; i++)
//            delay_ms(10);
//    }
//}

void SET_POS(uint16_t x,uint16_t y)
{
    IIC_Write_cmd(0xB0+y);    //set page address
    IIC_Write_cmd((0x0f&(x+2)));    //set lower column address
    IIC_Write_cmd(((0xf0&(x+2))>>4)|0x10);    //set higher column address
}

void ShowCH(uint16_t x,uint16_t y,unsigned char *s)
{
    uint16_t l = 0;
    while(*s)
    {
        if(*s < 0x80)//ascii
        {
            ShowChar(x+l,y,*s);
            l+=8;
            s+=1;
        }
        else
        {
            ShowGB1616(x+l,y,(unsigned char *)s);
            l+=16;
            s+=2;
        }
    }
}



//16*16
void ShowGB1616(uint16_t x,uint16_t y,unsigned char c[2])
{
    uint16_t i,j;
    for(i=0; i<FONTGB16NUM; i++)
    {
        if(c[0] == codeGB16[i].index[0] && c[1] == codeGB16[i].index[1])
        {
            SET_POS(x,y);
            for(j=0; j<16; j++)
                IIC_Write_data(codeGB16[i].buf[j]);

            SET_POS(x,y+1);
            for(j=0; j<16; j++)
                IIC_Write_data(codeGB16[i].buf[j+16]);
        }
    }
}

//8*16
void ShowChar(uint16_t x,uint16_t y,signed char c)
{
    uint16_t i;
    c = c-' ';
    SET_POS(x,y);
    for(i=0; i<8; i++)
        IIC_Write_data(F8X16[c*16+i]);
    SET_POS(x,y+1);
    for(i=0; i<8; i++)
        IIC_Write_data(F8X16[c*16+i+8]);
}

// 8*16
void Colour_Reverse(uint16_t x,uint16_t y,uint8_t colour)  // 0hei  ffbai 
{
    uint8_t i;
    SET_POS(x,y);
    for(i=0;i<8;i++)
        IIC_Write_data(colour);
    SET_POS(x,y+1);
    for(i=0; i<8; i++)
        IIC_Write_data(colour);
}

  // // 1306
//void Set_Roll(uint8_t direction,uint8_t y_start,uint8_t y_end,uint8_t speed)
//{
//    IIC_Write_cmd(0x2E);

//    IIC_Write_cmd(0x2F);
//    
//    IIC_Write_cmd(0x26+direction);
//    
//    IIC_Write_cmd(0x00);
//    
//    IIC_Write_cmd(y_start);

//    IIC_Write_cmd(speed);

//    IIC_Write_cmd(y_end);
//    
//    
//}




void OLED_DrawBMP(unsigned char bmp[])
{ 	
 unsigned int j=0;
 unsigned char x,y;
  
	for(y=0;y<8;y++)
	{
        IIC_delay(100);
		SET_POS(0,y);
        IIC_delay(100);
        for(x=0;x<128;x++)
	    {      
	    	IIC_Write_data(bmp[j++]);	    
            IIC_delay(1);            
	    }
        
	}
} 



