#include "ds1302.h"
#include "debug.h"
#include "head.h"
Timer_Type_t time;

void DS1302_Delay(void)
{
    uint8_t i;
    for(i=5;i>0;i--)
    {
        //_nop();
    }
}


//写一个字节
void Write_Ds1302(uint8_t temp) 
{
    unsigned char i;
    SDA_WRITE;
    DS1302_Delay();
    for(i = 8;i > 0;i --)
    {
        if(temp & 0x01)
        {
            SDA = 1;
        }
        else
        {
            SDA = 0;
        }
        temp >>= 1;
        SCK = 1;
        DS1302_Delay();
        SCK = 0;
        DS1302_Delay();
    }

}  

uint8_t  Read_Ds1302(void)//读一个字节
{
    uint8_t  i = 0;
    uint8_t dat = 0;	
    SDA_READ;
    DS1302_Delay();
    for(i = 8;i > 0;i --)
    {
        dat >>= 1;
        if(SDA == 1)
        {
            dat |= 0x80;
        }
        else
        {
            dat &= ~0x80;
        }
        SCK = 1; //下降沿读取数据
        DS1302_Delay();
        SCK = 0;
        DS1302_Delay();
    }
    return dat;
}



// 写入一个字节的数据
void Write_Ds1302_Byte( uint8_t address,uint8_t dat )     
{
 	RST=0;	
 	//DS1302_Delay();
 	SCK=0;	
 	//DS1302_Delay();
 	RST=1; 	
 	//DS1302_Delay();  
 	Write_Ds1302(address);	
 	Write_Ds1302(dat);		
 	RST=0; 
       //DS1302_Delay();
       SCK=1;	 
}


//读取一个字节的数据
uint8_t Read_Ds1302_Byte (uint8_t address )
{  
 	uint8_t temp=0x00;
 	RST=0;	
 	//DS1302_Delay();
 	SCK=0;	
 	//DS1302_Delay();
 	RST=1;	
 	//DS1302_Delay();
 	Write_Ds1302(address);
        temp = Read_Ds1302();
        //DS1302_Delay();
        RST=0;	
	return (temp);			
}




uint8_t  bcd_to_dec(uint8_t bcd)//BCD码转10进制函数
{
    return ((bcd/16)*10+(bcd%16));	
}
 
uint8_t dec_to_bcd(uint8_t dec)//10进制转BCD码
{
    return ((dec/10)*16+(dec%10));
}

void Show_Time(void)
{
    ShowChar(10,1,(time.hour/10)+' ');
    ShowChar(30,1,(time.hour%10)+' ');
    
    ShowChar(10,3,(time.minute/10)+' ');
    ShowChar(30,3,(time.minute%10)+' ');
    
    ShowChar(10,5,(time.second/10)+' ');
    ShowChar(30,5,(time.second%10)+' ');
}

uint8_t hour=22,minute=21,second=20;

//将初始时间写入芯片
void WriteTime_DS1302(void)
{
	Write_Ds1302_Byte(0x8e,0x00);  	//允许写

 Write_Ds1302_Byte(0x8C,dec_to_bcd(time.year));    //年
    Write_Ds1302_Byte(0x88,dec_to_bcd(time.month));   //月
    Write_Ds1302_Byte(0x86,dec_to_bcd(time.day));		//日
 
    Write_Ds1302_Byte(0x8A,dec_to_bcd(time.wday));    //星期
   
	Write_Ds1302_Byte(0x84,dec_to_bcd(time.hour));  // 时
	Write_Ds1302_Byte(0x82,dec_to_bcd(time.minute));  // 分
	Write_Ds1302_Byte(0x80,dec_to_bcd(time.second)); // 秒 
	
	Write_Ds1302_Byte(0x8e,0x80); // 禁止写
}



//读取时间
void ReadTime_DS1302(void)
{
    uint8_t DATE_X = 0,DATE_Y = 1;
    uint8_t TIME_X = 22,TIME_Y = 5;
    
    time.second = bcd_to_dec(Read_Ds1302_Byte(0x81)&0x7F);	//秒
    time.minute = bcd_to_dec(Read_Ds1302_Byte(0x83));	//分
    time.hour = bcd_to_dec(Read_Ds1302_Byte(0x85)&0x3F);	  	//时
    time.day = bcd_to_dec(Read_Ds1302_Byte(0x87));	  	//日
    time.month = bcd_to_dec(Read_Ds1302_Byte(0x89));		//月	
    //time.wday = bcd_to_dec(Read_Ds1302_Byte(0x8B));		//星期	
    time.year = bcd_to_dec(Read_Ds1302_Byte(0x8D));	  	//年
 
    time.wday = week_conversion(time.year,time.month,time.day); //星期	

//DATA
    ShowChar(DATE_X,DATE_Y,(time.year/10)+'0');
    DATE_X+=8;
    ShowChar(DATE_X,DATE_Y,(time.year%10)+'0');
    DATE_X+=8;
    ShowChar(DATE_X,DATE_Y,'-');
    DATE_X+=8;
    ShowChar(DATE_X,DATE_Y,(time.month/10)+'0');
    DATE_X+=8;
    ShowChar(DATE_X,DATE_Y,(time.month%10)+'0');
    DATE_X+=8;
    ShowChar(DATE_X,DATE_Y,'-');
    DATE_X+=8;
    ShowChar(DATE_X,DATE_Y,(time.day/10)+'0');
    DATE_X+=8;
    ShowChar(DATE_X,DATE_Y,(time.day%10)+'0');
    DATE_X+=22;
    ShowChar(DATE_X,DATE_Y,(time.wday%10)+'0');

//  TIME
    ShowChar(TIME_X,TIME_Y,(time.hour/10)+'0');
    TIME_X+=8;
    ShowChar(TIME_X,TIME_Y,(time.hour%10)+'0');
    TIME_X+=8;
    ShowChar(TIME_X,TIME_Y,':');
    TIME_X+=8;
    ShowChar(TIME_X,TIME_Y,(time.minute/10)+'0');
    TIME_X+=8;
    ShowChar(TIME_X,TIME_Y,(time.minute%10)+'0');
    TIME_X+=8;
    ShowChar(TIME_X,TIME_Y,':');
    TIME_X+=8;
    ShowChar(TIME_X,TIME_Y,(time.second/10)+'0');
    TIME_X+=8;
    ShowChar(TIME_X,TIME_Y,(time.second%10)+'0');
    TIME_X+=8;

    
}




uint8_t week_conversion(uint8_t year,uint8_t month,uint8_t day) //星期转换
{	
    uint16_t y = year + 2000;
    //一月和二月被当作前一年的
    if((month==1)||(month==2))
    {
        month+=12;
        y--;
    }
    //u8 week=(day+2*month+3*(month+1)/5+y+y/4-y/100+y/400)%7;
    //printf("new:%d.%d.%d\n",y,month,day);
    return (day+2*month+3*(month+1)/5+y+y/4-y/100+y/400)%7+1;

}

void Ds1302_Reset(void)
{
    time.year = 20;
    time.month = 8;
    time.day = 19;
    time.wday = week_conversion(time.year,time.month,time.day);
    time.hour = 15;
    time.minute = 29;
    time.second = 57;

    WriteTime_DS1302(); // 写入时间

    Write_Ds1302_Byte(0x8E,0x00);  // 允许写
    Write_Ds1302_Byte(0xFC,0x66);  // 写入 标志位  读 写 的地址不是同一个   读出来的地址 +1
    Write_Ds1302_Byte(0x8E,0x80); // 禁止写

    
}

void Ds1302_Init(void)
{
    uint8_t temp = 0;
    RST = 0;
    SCK = 0;
    temp = Read_Ds1302_Byte(0xFD);	//判断标志位
    if(temp != 0x66)
    {
        Ds1302_Reset(); //重新写入时间
    }


// 写入 测试
    Write_Ds1302_Byte(0x8E,0x00);	//允许写
    Write_Ds1302_Byte(0xFA,0x89);   	//写0x89
    Write_Ds1302_Byte(0x8E,0x80);    //禁止写
    
// 读取 测试
    temp = Read_Ds1302_Byte(0xFB);	
    if(temp == 0x89)            //读取写进去的值
    {
        // 测试 成功
        DEBUG_8(0x66);
    }
    else
    {
         // 测试 失败
         DEBUG_8(0x88);
    }

    ReadTime_DS1302();			//读取时间
    time.second &= 0x7F; 		// 秒种设置 防止异常
    Write_Ds1302_Byte(0x8E,0x00);	//允许写
    Write_Ds1302_Byte(0x80,dec_to_bcd(time.second)); // 重写秒
    Write_Ds1302_Byte(0x8E,0x80);    //禁止写
}



void SetTime_Handle(Key_Status_t key)
{
    static uint8_t DATE_X = 0,DATE_Y = 1;
    static uint8_t TIME_X = 22,TIME_Y = 5;
    static uint8_t index = 0,interval = 200;
    switch(key)
    {
        case KEY_UP:
            
            switch(index)
            {
                case 0:  // 年
                    if(time.year < 99)
                        time.year++;
                    else
                        time.year = 1;
                    ShowChar(DATE_X,DATE_Y,(time.year/10)+'0');
                    ShowChar(DATE_X+8,DATE_Y,(time.year%10)+'0');
                    break;
                case 1: // 月
                    if(time.month < 12)
                        time.month++;
                    else
                        time.month = 1;
                    ShowChar(DATE_X,DATE_Y,(time.month/10)+'0');
                    ShowChar(DATE_X+8,DATE_Y,(time.month%10)+'0');
                    break;
                case 2: // 日
                     if(time.day < 31)
                        time.day++;
                     else 
                        time.day=1;
                    ShowChar(DATE_X,DATE_Y,(time.day/10)+'0');
                    ShowChar(DATE_X+8,DATE_Y,(time.day%10)+'0');
                    break;
                case 3: // 时
                    if(time.hour < 23)
                        time.hour++;
                    else
                        time.hour = 0;
                    ShowChar(TIME_X,TIME_Y,(time.hour/10)+'0');
                    ShowChar(TIME_X+8,TIME_Y,(time.hour%10)+'0');
                    break;
                case 4: // 分
                    if(time.minute < 59)
                        time.minute++;
                    else 
                        time.minute = 0;
                    ShowChar(TIME_X,TIME_Y,(time.minute/10)+'0');
                    ShowChar(TIME_X+8,TIME_Y,(time.minute%10)+'0');
                    break;
                case 5: // 秒
                    if(time.second < 59)
                        time.second++;
                    else
                        time.second = 0;
                    ShowChar(TIME_X,TIME_Y,(time.second/10)+'0');
                    ShowChar(TIME_X+8,TIME_Y,(time.second%10)+'0');
                    break;
                default:
                    index = 0;
                    break;
                
            }
            
            break;
        case KEY_DOWN:
            switch(index)
            {
                case 0:  // 年
                    if(time.year > 1)
                        time.year--;
                    else 
                        time.year = 99;
                    ShowChar(DATE_X,DATE_Y,(time.year/10)+'0');
                    ShowChar(DATE_X+8,DATE_Y,(time.year%10)+'0');
                    break;
                case 1: // 月
                    if(time.month > 1)
                        time.month--;
                    else 
                        time.month = 12;
                    ShowChar(DATE_X,DATE_Y,(time.month/10)+'0');
                    ShowChar(DATE_X+8,DATE_Y,(time.month%10)+'0');
                    break;
                case 2: // 日
                     if(time.day > 1)
                        time.day--;
                     else 
                        time.day = 31;
                    ShowChar(DATE_X,DATE_Y,(time.day/10)+'0');
                    ShowChar(DATE_X+8,DATE_Y,(time.day%10)+'0');
                    break;
                case 3: // 时
                    if(time.hour > 0)
                        time.hour--;
                    else 
                        time.hour = 23;
                    ShowChar(TIME_X,TIME_Y,(time.hour/10)+'0');
                    ShowChar(TIME_X+8,TIME_Y,(time.hour%10)+'0');
                    break;
                case 4: // 分
                    if(time.minute > 0)
                        time.minute--;
                    else
                        time.minute = 59;
                    ShowChar(TIME_X,TIME_Y,(time.minute/10)+'0');
                    ShowChar(TIME_X+8,TIME_Y,(time.minute%10)+'0');
                    break;
                case 5: // 秒
                    if(time.second > 0)
                        time.second--;
                    else
                        time.second = 59;
                    ShowChar(TIME_X,TIME_Y,(time.second/10)+'0');
                    ShowChar(TIME_X+8,TIME_Y,(time.second%10)+'0');
                    break;
                default:
                    index = 0;
                    break;
                
            }
            break;
        case KEY_LEFT:
            if(index>0)
            {  
                 index--;
              /*   
                //根据 index 来翻转颜色
                if(index < 3)  //年月日
                {
                    if(index == 0)
                    {
                        //需要先将 前面翻转的颜色转回来  
                        Colour_Reverse(DATE_X,DATE_Y,0X00); // 翻回黑色
                        Colour_Reverse(DATE_X+8,DATE_Y,0X00); // 翻回黑色
                    
                        ShowChar(DATE_X,DATE_Y,(time.month/10)+'0');
                        ShowChar(DATE_X+8,DATE_Y,(time.month%10)+'0');

                        DATE_X -= 24;
                        Colour_Reverse(DATE_X,DATE_Y,0Xff); // 翻成白色
                        Colour_Reverse(DATE_X+8,DATE_Y,0Xff); // 翻成白色
                        delay_ms(interval);
                        ShowChar(DATE_X,DATE_Y,(time.year/10)+'0');
                        ShowChar(DATE_X+8,DATE_Y,(time.year%10)+'0');
                         
                    }
                    else if(index == 1)
                    {
                        //需要先将 前面翻转的颜色转回来  
                        Colour_Reverse(DATE_X,DATE_Y,0X00); // 翻回黑色
                        Colour_Reverse(DATE_X+8,DATE_Y,0X00); // 翻回黑色
                        
                        ShowChar(DATE_X,DATE_Y,(time.day/10)+'0');
                        ShowChar(DATE_X+8,DATE_Y,(time.day%10)+'0');

                        DATE_X -= 24;
                        Colour_Reverse(DATE_X,DATE_Y,0Xff); // 翻成白色
                        Colour_Reverse(DATE_X+8,DATE_Y,0Xff); // 翻成白色
                        delay_ms(interval);
                       ShowChar(DATE_X,DATE_Y,(time.month/10)+'0');
                       ShowChar(DATE_X+8,DATE_Y,(time.month%10)+'0');
                    }
                    else if(index == 2)
                    {
                        //需要先将 前面翻转的颜色转回来  
                        Colour_Reverse(TIME_X,TIME_Y,0X00); // 翻回黑色
                        Colour_Reverse(TIME_X+8,TIME_Y,0X00); // 翻回黑色
                        
                        ShowChar(TIME_X,TIME_Y,(time.hour/10)+'0');
                        ShowChar(TIME_X+8,TIME_Y,(time.hour%10)+'0');

                        Colour_Reverse(DATE_X,DATE_Y,0Xff); // 翻成白色
                        Colour_Reverse(DATE_X+8,DATE_Y,0Xff); // 翻成白色
                        delay_ms(interval);
                        ShowChar(DATE_X,DATE_Y,(time.day/10)+'0');
                        ShowChar(DATE_X+8,DATE_Y,(time.day%10)+'0');
                    }
                    
                }
                else //时分秒
                {
                     if(index == 3)
                     {
                        //需要先将 前面翻转的颜色转回来  
                        Colour_Reverse(TIME_X,TIME_Y,0X00); // 翻回黑色
                        Colour_Reverse(TIME_X+8,TIME_Y,0X00); // 翻回黑色
                    
                        ShowChar(TIME_X,TIME_Y,(time.minute/10)+'0');
                        ShowChar(TIME_X+8,TIME_Y,(time.minute%10)+'0');

                        TIME_X -=24;
                        Colour_Reverse(TIME_X,TIME_Y,0Xff); // 翻成白色
                        Colour_Reverse(TIME_X+8,TIME_Y,0Xff); // 翻成白色
                        delay_ms(interval);
                        ShowChar(TIME_X,TIME_Y,(time.hour/10)+'0');
                        ShowChar(TIME_X+8,TIME_Y,(time.hour%10)+'0');
                     }
                     else if(index == 4)
                     {
                        //需要先将 前面翻转的颜色转回来  
                        Colour_Reverse(TIME_X,TIME_Y,0X00); // 翻回黑色
                        Colour_Reverse(TIME_X+8,TIME_Y,0X00); // 翻回黑色
                    
                        ShowChar(TIME_X,TIME_Y,(time.second/10)+'0');
                        ShowChar(TIME_X+8,TIME_Y,(time.second%10)+'0');

                         TIME_X -=24;
                        Colour_Reverse(TIME_X,TIME_Y,0Xff); // 翻成白色
                        Colour_Reverse(TIME_X+8,TIME_Y,0Xff); // 翻成白色
                        delay_ms(interval);
                        ShowChar(TIME_X,TIME_Y,(time.minute/10)+'0');
                        ShowChar(TIME_X+8,TIME_Y,(time.minute%10)+'0');
                     }
                     else if(index == 5)
                     {
                        //不存在 
                     }
                }
                */  
            }
                         
            break;
        case KEY_RIGHT:
            if(index<5)
            {  
                index++;
                /*
                //根据 index 来翻转颜色
                if(index < 3)  //年月日
                {
                    if(index == 0) // 按下右 不可能 为0
                    {
                        //ShowChar(DATE_X,DATE_Y,(time.year/10)+'0');
                        //ShowChar(DATE_X+8,DATE_Y,(time.year%10)+'0');
                    }
                    else if(index == 1)
                    {
                        //需要先将 前面翻转的颜色转回来  
                        Colour_Reverse(DATE_X,DATE_Y,0X00); // 翻回黑色
                        Colour_Reverse(DATE_X+8,DATE_Y,0X00); // 翻回黑色
                    
                        ShowChar(DATE_X,DATE_Y,(time.year/10)+'0');
                        ShowChar(DATE_X+8,DATE_Y,(time.year%10)+'0');

                        DATE_X += 24;
                        Colour_Reverse(DATE_X,DATE_Y,0Xff); // 翻成白色
                        Colour_Reverse(DATE_X+8,DATE_Y,0Xff); // 翻成白色
                        delay_ms(interval);
                        ShowChar(DATE_X,DATE_Y,(time.month/10)+'0');
                        ShowChar(DATE_X+8,DATE_Y,(time.month%10)+'0');

                    
                    }
                    else if(index == 2)
                    {
                         //需要先将 前面翻转的颜色转回来  
                        Colour_Reverse(DATE_X,DATE_Y,0X00); // 翻回黑色
                        Colour_Reverse(DATE_X+8,DATE_Y,0X00); // 翻回黑色
                        
                        ShowChar(DATE_X,DATE_Y,(time.month/10)+'0');
                        ShowChar(DATE_X+8,DATE_Y,(time.month%10)+'0');

                        DATE_X += 24;
                        Colour_Reverse(DATE_X,DATE_Y,0Xff); // 翻成白色
                        Colour_Reverse(DATE_X+8,DATE_Y,0Xff); // 翻成白色
                        delay_ms(interval);
                        ShowChar(DATE_X,DATE_Y,(time.day/10)+'0');
                        ShowChar(DATE_X+8,DATE_Y,(time.day%10)+'0');
                    
                    }
                    
                }
                else //时分秒
                {
                    if(index == 3)  //前面是 日
                    {
                         //需要先将 前面翻转的颜色转回来  
                        Colour_Reverse(DATE_X,DATE_Y,0X00); // 翻回黑色
                        Colour_Reverse(DATE_X+8,DATE_Y,0X00); // 翻回黑色
                    
                        ShowChar(DATE_X,DATE_Y,(time.day/10)+'0');
                        ShowChar(DATE_X+8,DATE_Y,(time.day%10)+'0');

         
                        Colour_Reverse(TIME_X,TIME_Y,0Xff); // 翻成白色
                        Colour_Reverse(TIME_X+8,TIME_Y,0Xff); // 翻成白色
                        delay_ms(interval);
                        ShowChar(TIME_X,TIME_Y,(time.hour/10)+'0');
                        ShowChar(TIME_X+8,TIME_Y,(time.hour%10)+'0');                        
                    }
                    else if(index == 4) 
                    {
                        //需要先将 前面翻转的颜色转回来  
                        Colour_Reverse(TIME_X,TIME_Y,0X00); // 翻回黑色
                        Colour_Reverse(TIME_X+8,TIME_Y,0X00); // 翻回黑色
                    
                        ShowChar(TIME_X,TIME_Y,(time.hour/10)+'0');
                        ShowChar(TIME_X+8,TIME_Y,(time.hour%10)+'0');

                        TIME_X+=24;
                        Colour_Reverse(TIME_X,TIME_Y,0Xff); // 翻成白色
                        Colour_Reverse(TIME_X+8,TIME_Y,0Xff); // 翻成白色
                        delay_ms(interval);
                        ShowChar(TIME_X,TIME_Y,(time.minute/10)+'0');
                        ShowChar(TIME_X+8,TIME_Y,(time.minute%10)+'0');     
                        
                    }
                    else if(index == 5)
                    {
                        //需要先将 前面翻转的颜色转回来  
                        Colour_Reverse(TIME_X,TIME_Y,0X00); // 翻回黑色
                        Colour_Reverse(TIME_X+8,TIME_Y,0X00); // 翻回黑色
                    
                        ShowChar(TIME_X,TIME_Y,(time.minute/10)+'0');
                        ShowChar(TIME_X+8,TIME_Y,(time.minute%10)+'0');

                        TIME_X+=24;
                        Colour_Reverse(TIME_X,TIME_Y,0Xff); // 翻成白色
                        Colour_Reverse(TIME_X+8,TIME_Y,0Xff); // 翻成白色
                        delay_ms(interval);
                        ShowChar(TIME_X,TIME_Y,(time.second/10)+'0');
                        ShowChar(TIME_X+8,TIME_Y,(time.second%10)+'0');   
                    }   
                }
                */
            }

            break;
        case KEY_OK:
            index = 0;
            DATE_X = 0;
            DATE_Y = 1;
            TIME_X = 22;
            TIME_Y = 5;
            Sys_Status = SYS_RUNOVER;
            break;
        case KEY_OK_LONG:
             if(Sys_Status == SYS_RUNNING)
            {
                index = 0;
                DATE_X = 0;
                DATE_Y = 1;
                TIME_X = 22;
                TIME_Y = 5;
                WriteTime_DS1302();
                Sys_Status = SYS_RUNOVER;
            }
            break;
            
    }
}


//  -----------  alarm 

static volatile uint8_t Alarm_Flag = 0;
static volatile uint8_t Alarm_Step = 0;
static volatile uint16_t interval = 0;
void Clock_Alarm(void)
{
    if(time.hour == 15 && time.minute == 30 && time.second == 0 && time.wday != 7)
    {
        Alarm_Flag = 1;
        Alarm_Step = 1;
        return;
    }
    else if(time.minute != 30)
    {
        Alarm_Flag = 0;
        Alarm_Step = 0;
        return;
    }

    if(Alarm_Flag)
    {
        switch(Alarm_Step)
        {
            case 1:  //di
                Buzzer_sound(10);
                Alarm_Step++;
                break;
            case 2: //di结束
                if(Buzzer_Get() <= 3)
                {
                    interval = Get_SysTick_1ms();
                    Alarm_Step++;
                }
                break;
             case 3://间隔
                if(Get_SysTick_1ms() - interval >= 260)
                {
                    Alarm_Step++;
                }
                break;
            case 4:  //di
                Buzzer_sound(30);
                Alarm_Step++;
                break;
            case 5: //di结束
                if(Buzzer_Get() <= 3)
                {
                    interval = Get_SysTick_1ms();
                    Alarm_Step++;
                }
                break;
             case 6://间隔
                if(Get_SysTick_1ms() - interval >= 170)
                {
                    Alarm_Step++;
                }
                break;
             case 7:  //di
                Buzzer_sound(15);
                Alarm_Step++;
                break;
            case 8: //di结束
                if(Buzzer_Get() <= 3)
                {
                    interval = Get_SysTick_1ms();
                    Alarm_Step++;
                }
                break;
             case 9://间隔
                if(Get_SysTick_1ms() - interval >= 525)
                {
                    Alarm_Step = 1;
                }
                break;
            default:
                Alarm_Step = 0;
                break;
        }
        
    }
}

void Clean_Alarm(void)
{
    Alarm_Flag = 0;
    Alarm_Step = 0;
}

