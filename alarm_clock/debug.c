#include "debug.h"

#if DEBUG
void uart_char(unsigned char *data)
{
    while(*data)
    {
        while(!_txif);
        {
            _txr_rxr = *data;

        }
        data++;
    }
    while(!_txif);
}

void uart_send8(unsigned char data)
{
    unsigned char temp;
    while(!_txif);
    {
        _txr_rxr = '0';
    }

    while(!_txif);
    {
        _txr_rxr = 'X';
    }

    while(!_txif);
    {
        temp =  data/16;
        if(temp <=9 & temp>=0)
            _txr_rxr =  temp+'0';
        else if(temp <=15 &  temp >9)
            _txr_rxr =  temp-10+'A';

    }

    while(!_txif);
    {
        temp =  data%16;
        if(temp <=9 & temp>=0)
            _txr_rxr =  temp+'0';
        else if(temp <=15 &  temp >9)
            _txr_rxr =  temp-10+'A';

    }
    while(!_txif);
}



void uart_send16(unsigned short data)
{
    unsigned char temp;
    while(!_txif);
    {
        _txr_rxr = '0';
    }

    while(!_txif);
    {
        _txr_rxr = 'X';

    }


    while(!_txif);
    {
        temp =  (data)>>8;
        temp = temp/16;
        if(temp <=9 & temp>=0)
            _txr_rxr =  temp+'0';
        else if(temp <=15 &  temp >9)
            _txr_rxr =  temp-10+'A';

    }


    while(!_txif);
    {
        temp =  (data)>>8;
        temp = temp%16;
        if(temp <=9 & temp>=0)
            _txr_rxr =  temp+'0';
        else if(temp <=15 &  temp >9)
            _txr_rxr =  temp-10+'A';

    }

    while(!_txif);



    while(!_txif);
    {
        temp =  (data)&0xff;
        temp = temp/16;
        if(temp <=9 & temp>=0)
            _txr_rxr =  temp+'0';
        else if(temp <=15 &  temp >9)
            _txr_rxr =  temp-10+'A';
    }

    while(!_txif);
    {
        temp =  (data)&0xff;
        temp = temp%16;
        if(temp <=9 & temp>=0)
            _txr_rxr =  temp+'0';
        else if(temp <=15 &  temp >9)
            _txr_rxr =  temp-10+'A';

    }
    while(!_txif);

}
#endif


