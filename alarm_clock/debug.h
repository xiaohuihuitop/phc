#ifndef __DEBUG_H__
#define __DEBUG_H__

#include "head.h"

#define DEBUG 0

#if DEBUG
/*
    ���ڲ���
*/

void uart_char(unsigned char *data);
void uart_send8(unsigned char data);
void uart_send16(unsigned short data);
#endif


#if DEBUG
#define DEBUG_CHAR  uart_char
#define DEBUG_8         uart_send8
#define DEBUG_16       uart_send16
#else
#define DEBUG_CHAR  (void *)
#define DEBUG_8          (void *)
#define DEBUG_16         (void *)
#endif



#endif

