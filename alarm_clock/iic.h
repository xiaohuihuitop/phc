#ifndef __IIC_H__
#define __IIC_H__

#include "head.h"

//#define  IIC_MODE

#define IIC_SDA     _pb5
#define IIC_SCL      _pb6


//extern unsigned char  show1[];
//extern unsigned char  show2[];
//extern unsigned char  show3[];

//extern unsigned char  t[];
//extern unsigned char  c[];
//extern unsigned char  h[];

void IIC_Init(void);

void IIC_Send(uint8_t c);
void IIC_WaitAck(void);
void IIC_Start(void);
void IIC_Stop(void);
void IIC_Write_cmd(uint8_t cmd);
void IIC_Write_data(uint8_t data);
void IIC_delay(uint16_t i);
void  SSD1306(void);
void Fill(uint8_t start,uint8_t end,unsigned char dat1);
void Lcd_ON(void);
void Lcd_OFF(void);



void test(void);
void SET_POS(uint16_t x,uint16_t y);
void ShowCH(uint16_t x,uint16_t y,unsigned char *s);
void ShowGB1616(uint16_t x,uint16_t y,unsigned char c[2]);
void ShowChar(uint16_t x,uint16_t y,signed char c);
void Colour_Reverse(uint16_t x,uint16_t y,uint8_t colour);

void Set_Roll(uint8_t direction,uint8_t y_start,uint8_t y_end,uint8_t speed);


//----------------------

void OLED_DrawBMP(unsigned char BMP[]);

#endif






