#ifndef __DS1302_H__
#define __DS1302_H__

#include "head.h"

#include "key.h"

#define SCK   _pb1
#define SDA   _pb2
#define RST  _pb0

#define SDA_READ  _pbc2=1
#define SDA_WRITE _pbc2=0


typedef struct{
    uint8_t year;
    uint8_t month;
    uint8_t day;
    uint8_t wday;
    uint8_t hour;
    uint8_t minute;
    uint8_t second;
}Timer_Type_t;

uint8_t week_conversion(uint8_t year,uint8_t month,uint8_t day); //����ת��

void WriteTime_DS1302(void);
void ReadTime_DS1302(void);
void Ds1302_Init(void);
void SetTime_Handle(Key_Status_t key);

void Clock_Alarm(void);
void Clean_Alarm(void);
#endif

